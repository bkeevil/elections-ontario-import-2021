/** This module provides methods for calculating vote summaries on the
 *  data structure provided by the db object.
 */
var db = require('./database').db;

class SummaryCalculator {

  execute() {
    this.clearVoteSummaries();
    this.calculateVoteSummaries();
    this.lookupCandidateParties();
  }

  clearVoteSummaries() {
    db.events.forEach((event) => {
      event.parties.forEach((party) => {
        party.votes = 0;
      });
      event.totalUnmarked = 0;
      event.totalRejected = 0;
      event.totalDeclined = 0;
      event.numElectors = 0;
    });  

    db.elections.forEach((election) => {
      election.totalUnmarked = 0;
      election.totalRejected = 0;
      election.totalDeclined = 0;
      election.totalVotes = 0;
      election.candidates.forEach((candidate) => {
        //candidate.votes = 0;
        candidate.percent *= 100;
      });
      election.pollResults.forEach((pollResult) => {
        pollResult.totalVotes = 0;
      });
    });    
  }

  calculateVoteSummaries() {
    console.log("Calculating vote summaries");
    db.elections.forEach((election) => {
      election.pollResults.forEach((pollResult) => {
        for (let idx=0;idx<pollResult.votes.length;idx++) {
          let vote = pollResult.votes[idx];
          pollResult.totalVotes += vote.count;
        }
        for (let idx=0;idx<pollResult.votes.length;idx++) {
          let vote = pollResult.votes[idx];
          vote.percent = vote.count / (pollResult.totalVotes + pollResult.declined) * 100;
        }
        if (pollResult.numElectors == 0) {
          pollResult.turnout = null;
        } else {
          pollResult.turnout = (pollResult.totalVotes + pollResult.declined + pollResult.rejected + pollResult.unmarked) / pollResult.numElectors * 100;
        }

        election.numElectors += pollResult.numElectors;
        election.totalVotes += pollResult.totalVotes;
        election.totalDeclined += pollResult.declined;
        election.totalRejected += pollResult.rejected;
        election.totalUnmarked += pollResult.unmarked;
      });

      if (election.numElectors == 0) {
        election.turnout = null;
      } else {
        election.turnout = (election.totalVotes + election.totalDeclined + election.totalRejected + election.totalUnmarked) / election.numElectors * 100;
      }

      election.event.totalVotes += election.totalVotes;
      election.event.totalDeclined += election.totalDeclined;
      election.event.totalRejected += election.totalRejected;
      election.event.totalUnmarked += election.totalUnmarked;
      election.event.numElectors += election.numElectors;

      election.candidates.forEach((candidate) => {
        let party = election.event.parties.find({ code: candidate.party });
        party.votes += candidate.votes;
      });
    
    });

    db.events.forEach((event) => {
      let total = (event.totalVotes + event.totalDeclined);
      event.parties.forEach((party) => {
        party.percent = party.votes / total * 100;
      });
      if (event.numElectors == 0)
        event.turnout = null;
      else {
        total += event.totalUnmarked + event.totalRejected;
        event.turnout = total / event.numElectors * 100;
      }
    });

  }

  lookupCandidateParties() {
    db.elections.forEach((election) => {
      election.candidates.sort();
      election.pollResults.forEach((pollResult) => {
        pollResult.votes.sort();
        pollResult.votes.forEach((vote,idx) => {
          vote.party = election.candidates[idx].party;
        });
      });
    });
  }

}

var summaryCalculator = new SummaryCalculator();

module.exports = summaryCalculator;
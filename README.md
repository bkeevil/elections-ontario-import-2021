# 2021 Elections Ontario Data Converter

This Node.js (JavaScript) application converts Elections Ontario data into a set of GeoJSON map layers and a MongoDB database for serving them.

`node import.js` does the following:

1. Imports Elections Ontario election result data from their CSV files. 
2. Imports Elections Ontario district and polling division shape files.
3. Organizes that data in an in-memory data structure and calculates some additional summary data.
4. Saves the in-memory data structure to a directory

`node export.js` does the following:
1. Loads the in-memory data structure from a directory
2. Exports the data as a set of GeoJSON files for use in GIS and mapping applications.
3. Exports the data into a MongoDB database for use in a web API backend.

## Scope

The scope of this project should be limited to the data provided by Elections Ontario. Properietary data or data from other source (i.e Statistics Canada) should not be included in this project.

## Ownership

This source code is owned by the Ontario Libertarian Party. 

The data it converts is owned by Elections Ontario.

## Author

```
Bond Keevil, 
System Administrator
Ontario Libertarian Party
bond.keevil@libertarian.on.ca
```

## Contributions

We have made this import/export process an open source project as it is fairly generic and other media outlets, researchers and political parties might it useful.

Code contributions and bug reports are welcome.

## Licensing

This source code is released under the MIT license, as it is the most permissive. You do not need our permission to use it and you don't have to give the author or our party any attribution credits.

Elections Ontario has its own license agreement for the use of their data. Please see their website for their current license terms.

## Installation and Operation

1. Install node, npm and mongodb
2. Install dependencies by executing `npm install` in the project root.
3. Download and extract into `/Elections Ontario Data` the files from https://cloud.libertarian.on.ca/index.php/s/CHLQdJ2M8MJxQiE
4. Download and extract into `/shapefiles` the files from https://cloud.libertarian.on.ca/index.php/s/SLBr8oA4ADGKm4r
5. Run the import process with `node import.js`.
6. Run the export process with `node export.js`.

## Directories

/Elections Ontario Data - the CSV data provided by Elections Ontario

/shapefiles - the election boundary shapefiles provided by Elections Ontario

/json - The contents of the in-memory database. It is saved/loaded to/from this directory

/maps - The GeoJSON maps output as individual files. These can be viewed in QGIS.

## MongoDB Output

The application tries to create a MongoDB database called "MapDB" on the server localhost. Authentication is not supported. You must have MongoDB installed in order for this to work.

## Limitations and Known Issues

The application stores all the data in RAM during the import. This might be a problem on older computers. I estimated it required around 3GB of RAM to run.

Data needs more validation. Especially with regards to the handling of combined or split polls.
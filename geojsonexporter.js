/** Exports data from the memory data store into a set of geoJSON map files  */
const fs    = require('fs');
const path = require('path');
const db    = require('./database').db;
const gdal  = require('gdal-next');

var ref4326 = gdal.SpatialReference.fromEPSGA(4326);
const crs4326 = { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } };

class GeoJSON {
  name;
  type = "FeatureCollection";
  crs = crs4326;
  bbox = [];
  features = [];

  constructor(name) {
    this.name = name;
  }

  clone() {
    let result = new GeoJSON(this.name);
    result.bbox.length = this.bbox.length;
    for (let idx=0; idx < this.bbox.length; idx++) {
      result.bbox[idx] = this.bbox[idx];
    }
    result.features.length = this.features.length;
    for (let idx=0; idx < this.features.length; idx++) {
      result.features[idx] = this.features[idx].clone();
    }
    return result;
  }

  saveToFile(filename) {
    console.log("Saving:",filename);
    let data = JSON.stringify(this,null,"  ");
    fs.writeFileSync(filename,data);
  }

  updateFeatureBounds() {
    if (this.features.length > 0) {
      this.bbox = [this.features[0].bbox[0],this.features[0].bbox[1],this.features[0].bbox[2],this.features[0].bbox[3]];
      for (let idx=1; idx < this.features.length; idx++) {
        let bbox = this.features[idx].bbox;
        if (bbox[0] < this.bbox[0]) this.bbox[0] = bbox[0];
        if (bbox[1] < this.bbox[1]) this.bbox[1] = bbox[1];
        if (bbox[2] > this.bbox[2]) this.bbox[2] = bbox[2];
        if (bbox[3] > this.bbox[3]) this.bbox[3] = bbox[3];
      }
    }
  }

}

class GeoJSONFeature {
  id;
  type = "Feature";
  properties;
  bbox;
  geometry;

  constructor(id,properties,bbox,geometry) {
    this.id = id;
    this.properties = properties;
    this.bbox = bbox;
    this.geometry = geometry
  }

  clone() {
    let bbox = new Array(this.bbox.length);
    for (let idx=0; idx < this.bbox.length; idx++) {
      bbox[idx] = this.bbox[idx];
    }
    return new GeoJSONFeature(this.id,JSON.parse(JSON.stringify(this.properties)),bbox,this.geometry);
  }

}

class GeoJSONCreator {
  flatprops = false;

  makeDistrictGeoJSON(year,geoms) { 
    console.log(`Creating ${year} District GeoJSON Map Template`);
    let result = new GeoJSON(`${year} Districts`);

    for (let idx=0; idx < geoms.length; idx++) {
      let geom = geoms[idx];      
      let properties = this.flatprops 
        ? { map_year: year, map_type: "ED", district_number: geom.edno, district_area: geom.area }
        : { map: { year: year, type: "ED" }, district: { number: geom.edno, area: geom.area } };       
      let gdalgeom = gdal.Geometry.fromWKT(geom.geom,ref4326);
      let geometry = JSON.parse(gdalgeom.toJSON());
      let bbox = [geom.bbox.minX, geom.bbox.minY, geom.bbox.maxX, geom.bbox.maxY];      
      let feature = new GeoJSONFeature(geom.edno,properties,bbox,geometry);
      result.features.push(feature)
    }
    result.updateFeatureBounds();
    return result;  
  }

  makePollDivisionGeoJSONs(year,geoms) { 
    console.log(`Creating ${year} Poll Division GeoJSON Map Templates`);
    let results = [];

    for (let idx=0; idx < geoms.length; idx++) {
      let geom = geoms[idx];
      while (results.length < geom.edno) {
        results.push(new GeoJSON(`${year} District ${results.length+1} Poll Divisions`));
      }
      let result = results[geom.edno - 1];
      
      let bbox = [geom.bbox.minX, geom.bbox.minY, geom.bbox.maxX, geom.bbox.maxY];
      let properties = this.flatprops
        ? { map_year: year, map_type: "PD", district_number: geom.edno, polldivision_number: geom.pollno, polldivision_area: geom.area }
        : { map: { year: year, type: "PD" }, district: { number: geom.edno }, polldivision: { number: geom.pollno, area: geom.area } }
      let gdalgeom = gdal.Geometry.fromWKT(geom.geom,ref4326);    
      let geometry = JSON.parse(gdalgeom.toJSON());
      let feature = new GeoJSONFeature(geom.pollno,properties,bbox,geometry);
      result.features.push(feature)
    }
    for (let idx = 0; idx < results.length; idx++) {
      results[idx].updateFeatureBounds();
    }
    return results;  
  }

  execute() {
    var maps = {
      districts: {
        "2018": this.makeDistrictGeoJSON(2018,db.districtGeometry2018),
        "2014": this.makeDistrictGeoJSON(2014,db.districtGeometry2014),
      },
      pollDivisions: {
        "2018": this.makePollDivisionGeoJSONs(2018,db.pollDivisionGeometry2018),
        "2014": this.makePollDivisionGeoJSONs(2014,db.pollDivisionGeometry2014),
      }
    }
    return maps;
  }
}

class GeoJSONExporter {
  flatprops = false;

  addDistrictMapFeatureProperties(properties,election) {
    properties.unmarked_ballots = election.totalUnmarked;
    properties.rejected_ballots = election.totalRejected;
    properties.declined_ballots = election.totalDeclined;
    properties.num_electors = election.numElectors;
    properties.total_votes = election.totalVotes;
    properties.turnout = election.turnout;
    if (this.flatprops) {
      properties.event_year = election.event.year;
      properties.event_type = election.event.type;
      properties.district_number = election.district.number;
      properties.district_name = election.district.name;
      election.candidates.forEach((candidate) => {
        properties[candidate.party+"_name"] = candidate.name;
        properties[candidate.party+"_votes"] = candidate.votes;
        properties[candidate.party+"_percent"] = candidate.percent;
      });
    } else {
      properties.event = {
        year: election.event.year,
        type: election.event.type,
      },
      properties.district.number = election.district.number;
      properties.district.name = election.district.name;
      properties.candidates = [];
      election.candidates.forEach((candidate) => {
        let newcandidate = {
          name: candidate.name,
          party: candidate.party,
          votes: candidate.votes,
          percent: candidate.percent
        }
        properties.candidates.push(newcandidate);
      });
    }
  }

  deleteUnusedDistrictFeatures(geojson,elections) {
    let newfeatures = [];
    geojson.features.forEach((feature) => {
      for (let idx = 0; idx < elections.length; idx++) {
        if (elections[idx].district.number === feature.id) {
          newfeatures.push(feature);
          break;
        }
      }
    });
    geojson.features = newfeatures;
  }

  addDistrictProperties(geojson,elections) { 
    elections.forEach((election) => {
      let properties = null;
      for (let idx=0; idx < geojson.features.length; idx++) {
        let feature = geojson.features[idx];
        if (feature.id === election.district.number) {
          properties = feature.properties;
          break;
        }
      }
      if (properties !== null) {
        this.addDistrictMapFeatureProperties(properties,election);
      } else {
        console.error("provincial map feature properties not found");
      }
    });
  }

  makeDistrictGeoJSON(maps,event,elections) {
    let geojson;
    if (event.year >= 2018) {
      geojson = maps.districts["2018"].clone();
    } else {
      geojson = maps.districts["2014"].clone();
    }
    geojson.name = (event.type === 'GE') ? event.year + " General Election" : event.year + " Byelection";
    this.deleteUnusedDistrictFeatures(geojson,elections);
    this.addDistrictProperties(geojson,elections);
    return geojson;
  }

  addPollDivisionMapFeatureProperties(properties,election,pollResults) {
    //properties.pollcount = pollResults.length;
    
    if (this.flatprops) {
      properties.event_year = election.event.year;
      properties.event_type = election.event.type;
      properties.district_number = election.district.number;
      properties.district_name = election.district.name;
      properties.polldivision_debug = JSON.stringify(pollResults);  
    } else {
      properties.event = {
        year: election.event.year,
        type: election.event.type,
      },
      properties.district.number = election.district.number;
      properties.district.name = election.district.name;
      properties.polldivision.debug = JSON.stringify(pollResults);
    }

    if (pollResults.length > 0) {
      //properties.pollno = pollResults[0].pollno;
      
      // Calculate total votes
      let totalVotes = 0;
      let totalDeclined = 0;
      let totalRejected = 0;
      let totalUnmarked = 0;
      let numElectors = 0;
      let locations = [];
      pollResults.forEach((pollResult) => {
        totalVotes += pollResult.totalVotes;
        totalDeclined += pollResult.declined;
        totalRejected += pollResult.rejected;
        totalUnmarked += pollResult.unmarked;
        numElectors += pollResult.numElectors;
        if (locations.indexOf(pollResult.location) != 0) {
          locations.push(pollResult.location);
        }
      });
      properties.total_votes = totalVotes;
      properties.declined_ballots = totalDeclined;
      properties.rejected_ballots = totalRejected;
      properties.unmarked_ballots = totalUnmarked;
      properties.num_electors = numElectors;
      properties.turnout = (totalVotes + totalDeclined + totalRejected + totalUnmarked) / numElectors * 100;
      properties.location = locations.join();

      let numCandidates = pollResults[0].votes.length;
      let candidates = new Array(numCandidates);
      for (let idx=0;idx<numCandidates;idx++) {
        let vote = pollResults[0].votes[idx];
        candidates[idx] = { name: vote.candidate, party: vote.party, votes: vote.count };
        for (let idy=1;idy<pollResults.length;idy++) {
          candidates[idx].votes += pollResults[idy].votes[idx].count;
        }
      }

      if (this.flatprops) {
        candidates.forEach((candidate) => {
          properties[candidate.party+"_name"] = candidate.name; 
          properties[candidate.party+"_votes"] = candidate.votes;
          properties[candidate.party+"_percent"] = candidate.votes / (totalVotes + totalDeclined) * 100;
        });
      } else {
        properties.candidates = [];
        candidates.forEach((candidate) => {
          let newcandidate = {
            name: candidate.name,
            party: candidate.party,
            votes: candidate.votes,
            percent: candidate.votes / (totalVotes + totalDeclined) * 100
          }
          properties.candidates.push(newcandidate);
        });
      }
    }
  }

  makePollDivisionGeoJSON(maps,election) {
    let geojson;
    if (election.event.year >= 2018) {
      geojson = maps.pollDivisions["2018"][election.district.number - 1].clone();
    } else {
      geojson = maps.pollDivisions["2014"][election.district.number - 1].clone();
    }
    let eventName = (election.event.type === 'GE') ? election.event.year + " General Election" : election.event.year + " Byelection";
    geojson.name = `${eventName} - ${election.district.name} (${election.district.number})`;
    
    for (let idx=0; idx < geojson.features.length; idx++) {
      let feature = geojson.features[idx];
      let pollResults = election.pollResults.select((pollResult) => {
        let result = (pollResult.taken && !pollResult.advance && !pollResult.combined &&
            (String(pollResult.pollno).localeCompare(String(feature.id)) == 0));
        result |= (pollResult.combined && (pollResult.combinedWith.localeCompare(String(feature.id)) == 0));
        return result;
      });
      this.addPollDivisionMapFeatureProperties(feature.properties,election,pollResults);
    }
    return geojson;
  }

  execute(maps) {
    db.events.forEach((event) => {
      if (event.year >= 2007) {
        if ((event.year > 2007) || (event.type != 'BE')) {
          let elections = db.elections.select((item) => (event.compare(item.event,event) == 0));
          elections.sort();
          let geojson = this.makeDistrictGeoJSON(maps,event,elections);
          geojson.saveToFile(path.join('.','maps',event.type+event.year+'.geojson'));
          elections.forEach((election) => {
            geojson = this.makePollDivisionGeoJSON(maps,election);
            geojson.saveToFile(path.join('.','maps',event.type+event.year+'-'+election.district.number+'.geojson'));
          });
        }
      }
    });
  }

}

module.exports = {
  geojsonCreator: new GeoJSONCreator(),
  geojsonExporter: new GeoJSONExporter()
}
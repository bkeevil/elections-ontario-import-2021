const db    = require('./database').db;
const mongo = require('mongodb');

const uri = "mongodb://localhost:27017/";
const dbname = "MapDB";

async function execute(maps) {
  let mongoClient = await mongo.MongoClient.connect(uri);
  try {
    let mongoDB = mongoClient.db(dbname); 
    
    let collection = await mongoDB.collection('maps');
    collection.deleteMany({});    
    await collection.dropIndexes();  

    console.log("Exporting to MongoDB: district maps");
    await collection.insertMany([maps.districts["2018"],maps.districts["2014"]])
    
    console.log("Exporting to MongoDB: 2018 poll division maps");
    docs = maps.pollDivisions["2018"];
    await collection.insertMany(docs);
    
    console.log("Exporting to MongoDB: 2014 poll division maps");
    docs = maps.pollDivisions["2014"];
    await collection.insertMany(docs);    

    let events = createEventList(db);
    await insertEventList(mongoDB,events);

    let parties = createPartyList(db,events);
    await insertPartyList(mongoDB,parties);

    let districts = createDistrictList(db,events);
    await insertDistrictList(mongoDB,districts);

    let candidates = createCandidateList(db,events,districts,parties);
    await insertCandidateList(mongoDB,candidates);

    let pollResults = createPollResultList(db,events,districts,candidates);
    await insertPollResultList(mongoDB,pollResults);

  } catch (err) {
    console.error(err);
  } finally {
    mongoClient.close();
  }
}

function createEventList(db) {
  let results = [];
  db.events.forEach((event) => {
    if ((event.year > 2007) || ((event.year === 2007) && (event.type === 'GE'))) {
      let result = {
        year: event.year,
        type: event.type,
        numElectors: event.numElectors,
        totalVotes: event.totalVotes,
        totalDeclined: event.totalDeclined,
        totalUnmarked: event.totalUnmarked,
        totalRejected: event.totalRejected,
        turnout: event.turnout
      }
      results.push(result);
    }
  });
  return results;
}

async function insertEventList(mongoDB,events) {
  console.log("Inserting events");
  let collection = await mongoDB.collection('events');
  collection.deleteMany({});    
  await collection.dropIndexes();  
  let result = await collection.insertMany(events);
  for (let idx = 0; idx < result.insertedCount; idx++) {
    events[idx]._id = result.insertedIds[idx];
  }
}

function lookupEventID(events,year,type) {
  for (let idx = 0; idx < events.length; idx++) {
    let event = events[idx];
    if ((event.year === year) && (event.type === type)) {
      return event._id;
    }
  };
  return null;
}

function createPartyList(db,events) {
  var results = [];
  db.events.forEach((event) => {
    let eventID = lookupEventID(events,event.year,event.type);
    if (eventID !== null) {
      event.parties.forEach((party) => {
        let result = {
          eventID: eventID,
          code: party.code,
          name: party.name,
          registered: party.registered,
          percent: party.percent,
          votes: party.votes
        }
        results.push(result);
      });
    }
  });
  return results;
}

async function insertPartyList(mongoDB,parties) {
  console.log("Inserting parties");
  let collection = await mongoDB.collection('parties');
  collection.deleteMany({});    
  await collection.dropIndexes();  
  let result = await collection.insertMany(parties);
  for (let idx = 0; idx < result.insertedCount; idx++) {
    parties[idx]._id = result.insertedIds[idx];
  }
}

function createDistrictList(db,events) {
  var results = [];
  db.elections.forEach((election) => {
    let eventID = lookupEventID(events,election.event.year,election.event.type);
    if (eventID !== null) {
      let result = {
        eventID       : eventID,
        name          : election.district.name,
        number        : election.district.number,
        numElectors   : election.numElectors,
        totalVotes    : election.totalVotes,
        totalUnmarked : election.totalUnmarked,
        totalRejected : election.totalRejected,
        totalUnmarked : election.totalUnmarked,
        totalDeclined : election.totalDeclined,
        turnout       : election.turnout
      }
      results.push(result);
    }
  });
  return results;
}

async function insertDistrictList(mongoDB,districts) {
  console.log("Inserting districts");
  let collection = await mongoDB.collection('districts');
  collection.deleteMany({});    
  await collection.dropIndexes();  
  let result = await collection.insertMany(districts);
  for (let idx = 0; idx < result.insertedCount; idx++) {
    districts[idx]._id = result.insertedIds[idx];
  }
}

function compareObjectID(a,b) {
  if ((a._bsontype === 'ObjectID') && (b._bsontype === 'ObjectID')) {
    let result;
    for (let idx = 0; idx < 12; idx++) {
      result = a.id[idx] - b.id[idx];
      if (result != 0)
        return result;
    }
    return 0;
  } else {
    thow ("compareObjectID: Invalid _bsontype");
  }
}

function lookupDistrictID(districts,eventID,number) {
  for (let idx = 0; idx < districts.length; idx++) {
    let district = districts[idx];
    let result = compareObjectID(district.eventID,eventID);
    if (result == 0) {
      result = district.number - number;
    }
    if (result == 0 )
      return district._id;
  }
  return null;
}

function lookupPartyID(parties,eventID,code) {
  for (let idx = 0; idx < parties.length; idx++) {
    let party = parties[idx];
    let result = compareObjectID(party.eventID,eventID);
    if (result == 0) {
      result = result = party.code.localeCompare(code);
    }
    if (result == 0 )
      return party._id;
  }
  return null;
}

function createCandidateList(db,events,districts,parties) {
  var results = [];
  db.elections.forEach((election) => {
    let eventID = lookupEventID(events,election.event.year,election.event.type);
    if (eventID !== null) {
      let districtID = lookupDistrictID(districts,eventID,election.district.number);
      election.candidates.forEach((candidate) => {
        let partyID = lookupPartyID(parties,eventID,candidate.party);
        let result = {
          eventID       : eventID,
          districtID    : districtID,
          partyID       : partyID,
          name          : candidate.name,
          incumbent     : candidate.incumbent,
          party         : candidate.party,
          percent       : candidate.percent,
          votes         : candidate.votes
        }
        results.push(result);
      });
    }
  });
  return results;
}

async function insertCandidateList(mongoDB,candidates) {
  console.log("Inserting candidates");
  let collection = await mongoDB.collection('candidates');
  collection.deleteMany({});    
  await collection.dropIndexes();  
  let result = await collection.insertMany(candidates);
  for (let idx = 0; idx < result.insertedCount; idx++) {
    candidates[idx].id = result.insertedIds[idx];
  }
}

function selectElectionCandidates(db,eventID,districtID,candidates) {
  var results = [];
  candidates.forEach((candidate) => {
    if ((compareObjectID(candidate.eventID,eventID) == 0) && (compareObjectID(candidate.districtID,districtID) == 0)) {
      results.push(candidate);
    }
  });
  return results;
}

function createPollResultList(db,events,districts,candidates) {
  var results = [];
  db.elections.forEach((election) => {
    let eventID = lookupEventID(events,election.event.year,election.event.type);
    if (eventID !== null) {
      let districtID = lookupDistrictID(districts,eventID,election.district.number);
      let electioncandidates = selectElectionCandidates(db,eventID,districtID,candidates);
      election.pollResults.forEach((pollResult) => {
        let result = {
          eventID       : eventID,
          districtID    : districtID,
          pollcode      : pollResult.pollcode,
          pollno        : pollResult.pollno,
          advance       : pollResult.advance,
          combined      : pollResult.combined,
          combinedWith  : pollResult.combinedWith,
          declined      : pollResult.declined,
          location      : pollResult.location,
          numElectors   : pollResult.numElectors,
          rejected      : pollResult.rejected,
          taken         : pollResult.taken,
          totalVotes    : pollResult.totalVotes,
          turnout       : pollResult.turnout,
          unmarked      : pollResult.unmarked,
          votes         : []
        }
        pollResult.votes.forEach((vote) => {
          let candidateid = null;
          let partyid = null;
          for (let idx = 0; idx < electioncandidates.length; idx++) {
            let electioncandidate = electioncandidates[idx];
            if (electioncandidate.name.localeCompare(vote.candidate) == 0) {
              candidateid = electioncandidate._id;
              partyid = electioncandidate.partyID;
              break;
            }
          }
          let candidateVote = {
            candidateid: candidateid,
            count: vote.count,
            partyid: partyid,
            percent: vote.percent,
          }
          result.votes.push(candidateVote);
        });

        results.push(result);
      });
    }
  });
  return results;
}

async function insertPollResultList(mongoDB,pollResults) {
  console.log("Inserting pollresults");
  let collection = await mongoDB.collection('pollresults');
  collection.deleteMany({});    
  await collection.dropIndexes();  
  let result = await collection.insertMany(pollResults);
  for (let idx = 0; idx < result.insertedCount; idx++) {
    pollResults[idx].id = result.insertedIds[idx];
  }
}

module.exports = {
  execute: execute
}
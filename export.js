var fs                = require('fs');
var path              = require('path');
var db                = require('./database').db;
var geojsonCreator    = require('./geojsonexporter').geojsonCreator;
var geojsonExporter   = require('./geojsonexporter').geojsonExporter;
var mongoExporter     = require('./mongoexporter');

fs.mkdirSync(path.join('.','maps'),{ recursive: true });
db.loadFromDirectory(path.join('.','json'));

geojsonCreator.flatprops = false;   // Change to true to create geoJSONs without nested properties
geojsonExporter.flatprops = false;
let maps = geojsonCreator.execute();
geojsonExporter.execute(maps);
mongoExporter.execute(maps);
var fs   = require('fs');
var path = require('path');
var gdal = require('gdal-next');
var db   = require('./database').db;

const DistrictGeometry = require('./database').DistrictGeometry;
const PollDivisionGeometry = require('./database').PollDivisionGeometry;
const DistrictGeometryIntersect = require('./database').DistrictGeometryIntersect;
const PollDivisionGeometryIntersect = require('./database').PollDivisionGeometryIntersect;

// We need to convert each layer from EPSG:5320 to EPSG:4326  
var ref5320 = gdal.SpatialReference.fromEPSGA(5320);
var ref4326 = gdal.SpatialReference.fromEPSGA(4326);
var transformation = new gdal.CoordinateTransformation(ref5320,ref4326);

class DistrictImporter {
  #year;
  #geometry;

  constructor (year) {
    this.#year = year;
    if (this.#year == 2018) 
      this.#geometry = db.districtGeometry2018;
    else
      this.#geometry = db.districtGeometry2014;  
  }

  import(filename) {
    filename = filename;
    console.log("Importing shapefile:",filename);
    let dataset = gdal.open(filename);
    let layer = dataset.layers.get(0);
    let count = 0;

    layer.features.forEach((feature) => {
      let edno = feature.fields.get('ED_ID');
      let geom = feature.getGeometry();
      let area = 0;
      if (geom && geom.getArea) 
        area = geom.getArea();
      geom.transform(transformation);
      geom.swapXY();
      let bbox = geom.getEnvelope();
      let item = new DistrictGeometry(edno,area,bbox,geom.toWKT());
      this.#geometry.push(item);
      count++;
    });
    console.log(count,"districts imported");
    this.#geometry.sort();
    if (dataset) dataset.close();
  }
} 

class DistrictIntersecter {
  execute(filename2014,filename2018) {
    console.log("Intersection shapefile:",filename2014,"and",filename2018);
    let dataset2014 = gdal.open(filename2014);
    let layer2014 = dataset2014.layers.get(0);
    let dataset2018 = gdal.open(filename2018);
    let layer2018 = dataset2018.layers.get(0);
    let count = 0;
    layer2014.features.forEach((feature2014) => {
      let edno2014 = feature2014.fields.get('ED_ID');
      let geom2014 = feature2014.getGeometry();
      layer2018.features.forEach((feature2018) => {
        let edno2018 = feature2018.fields.get('ED_ID');
        let geom2018 = feature2018.getGeometry();
        let geomi = geom2014.intersection(geom2018);
        let areai = 0;
        if (geomi && geomi.getArea) {
          areai = geomi.getArea();
          if (areai > 1000) {
            geomi.transform(transformation);
            geomi.swapXY();
            let bboxi = geomi.getEnvelope();
            let item = new DistrictGeometryIntersect(edno2014,edno2018,areai,bboxi,geomi.toWKT());
            db.districtGeometryIntersects.push(item);
            console.log("Election",edno2014,"intersects with",edno2018);
            count++;
          }
        }
      });
    });
    console.log(count,"districts intersections imported");
    db.districtGeometryIntersects.sort();
    if (dataset2014) dataset2014.close();
    if (dataset2018) dataset2018.close();
  }
}

class PollDivisionImporter {
  #year;
  #geometry;
  
  constructor (year) {
    this.#year = year;
    if (this.#year == 2018) 
      this.#geometry = db.pollDivisionGeometry2018;
    else
      this.#geometry = db.pollDivisionGeometry2014;
  }

  import(filename) {
    console.log("Importing shapefile:",filename);
    let dataset = gdal.open(filename);
    let layer = dataset.layers.get(0);
    let count = 0;
    layer.features.forEach((feature) => {
      let edno = feature.fields.get('ED_ID');
      let pollno;
      if (this.#year == 2018) 
        pollno = feature.fields.get('PD_NUMBER');
      else 
        pollno = feature.fields.get('POLL_DIV_1');
      let area = 0;
      let geom = feature.getGeometry();
      if (geom && geom.getArea) {
        area = geom.getArea();
      }
      geom.transform(transformation);
      geom.swapXY();
      let bbox = geom.getEnvelope();
      let item = new PollDivisionGeometry(edno,pollno,area,bbox,geom.toWKT());
      this.#geometry.push(item);
      count++;
    });
    this.#geometry.sort();
    console.log(count,"poll divisions imported");
    if (dataset) dataset.close();    
  }
} 

class PollDivisionIntersecter {
  getIntersectingDistricts(edno) {
    let items = new Array();
    db.districtGeometryIntersects.forEach((dgi) => {
      if (dgi.edno2014 == edno) {
        items.push(dgi.edno2018);
      }
    });
    return items;
  }

  execute(filename2014,filename2018) {
    console.log("Intersection shapefile:",filename2014,"and",filename2018);
    let dataset2014 = gdal.open(filename2014);
    let layer2014 = dataset2014.layers.get(0);
    let dataset2018 = gdal.open(filename2018);
    let layer2018 = dataset2018.layers.get(0);
    let count = 0;
    let lastedno2014 = -1;
    let intersectingDistricts;
    layer2014.features.forEach((feature2014) => {
      let edno2014 = feature2014.fields.get('ED_ID');
      let pollno2014 = feature2014.fields.get('PD_NUMBER');
      let geom2014 = feature2014.getGeometry();
      if (edno2014 != lastedno2014) {
        intersectingDistricts = getIntersectingDistricts(edno2014);
        lastedno2014 = edno2014;
      }
      layer2018.features.forEach((feature2018) => {
        let edno2018 = feature2018.fields.get('ED_ID');
        if (intersectingDistricts.indexof(edno2018) != -1) {
          let pollno2018 = feature2018.fields.get('POLL_DIV_1')
          let geom2018 = feature2018.getGeometry();
          let geomi = geom2014.intersection(geom2018);
          let areai = 0;
          if (geomi && geomi.getArea) {
            areai = geomi.getArea();
            if (areai > 500) {
              geomi.transform(transformation);
              geomi.swapXY(); 
              let bboxi = geomi.getEnvelope();
              console.log(edno2014,'-',pollno2014,'intersects with',edno2018,'-',pollno2018);
              let item = new PollDivisionGeometryIntersect(edno2014,pollno2014,edno2018,pollno2018,area,bbox,geomi.toWKT());
              db.pollDivisionGeometryIntersects.push(item);
              count++;
            }
          }
        }
      });
    });
    console.log(count,"poll division intersections imported");
    db.pollDivisionGeometryIntersects.sort();
    if (dataset2014) dataset2014.close();
    if (dataset2018) dataset2018.close();
  }
}

class SHPImporter {
  ed2018;
  ed2014;
  edint;
  pd2018;
  pd2014;
  pdint;
  constructor() {
    this.ed2018 = new DistrictImporter(2018);
    this.ed2014 = new DistrictImporter(2014);
    this.edint  = new DistrictIntersecter();
    this.pd2018 = new PollDivisionImporter(2018);
    this.pd2014 = new PollDivisionImporter(2014);
    this.pdint  = new PollDivisionIntersecter();
  }
  import() {
    var filenameED2014 = path.join('.','shapefiles','Electoral District Shapefile - 2014 General Election','EDs_Ontario.shp');
    var filenameED2018 = path.join('.','shapefiles','Electoral District Shapefile - 2018 General Election','ELECTORAL_DISTRICT.shp');
    var filenamePD2014 = path.join('.','shapefiles','Polling Division Shapefile - 2014 General Election','PDs_Ontario.shp');
    var filenamePD2018 = path.join('.','shapefiles','Polling Division Shapefile - 2018 General Election','POLLING_DIVISION.shp');
    this.ed2014.import(filenameED2014);
    this.ed2018.import(filenameED2018);
    this.pd2014.import(filenamePD2014);
    this.pd2018.import(filenamePD2018); 
    //this.edint.execute(filenameED2014,filenameED2018);
    //this.pdint.execute(filenamePD2014,filenamePD2018);
  }
}

var shapefiles = new SHPImporter();

module.exports = shapefiles;
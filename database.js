/** This module provides the application's structured datastore.
 *  The data is stored in memory. 
 *  The datastore can be saved/loaded to/from a directory.
 */
const fs    = require('fs');
const path  = require('path');

class ObjectList extends Array {
  #itemClass;
  #listClass;
  #sorted;
  
  constructor (itemClass,listClass) {
    super();
    this.#itemClass = itemClass;
    this.#listClass = listClass;
    this.#sorted = false;
  }

  get sorted() { return this.#sorted; }
  set sorted(value) { 
    if (this.#sorted != value) {
      if (value) 
        this.sort();
      else
        this.#sorted = false;        
    } 
  }

  clear() { 
    this.forEach((item) => item.clear());
    this.length = 0;
  }
  
  sort() {
    if (!this.#sorted) {
      super.sort(this.#itemClass.prototype.compare);
      this.#sorted = true;
    }
  }
  
  push(item) {
    super.push(item);
    this.#sorted = false;
  }
  
  #binsearch(key) {
    let start=0;
    let end=this.length - 1;        
    let mid=Math.floor((start + end)/2);
    let cr = this.#itemClass.prototype.compare(this[mid],key);
    while ((cr != 0) && (start < end)) {  
      if (cr > 0)
        end = mid - 1;
      else if (cr < 0)
        start = mid + 1;
      mid = Math.floor((start + end)/2);
      cr = this.#itemClass.prototype.compare(this[mid],key);
    } 
    return (cr != 0) ? null : mid; 
  }
  
  #seqsearch(key) {
    for (let idx=0;idx<this.length;idx++) {
      if (this.#itemClass.prototype.compare(this[idx],key) == 0) {
        return idx;
      };
    }    
    return null;
  }

  indexof(key) {
    return this.#sorted ? this.#binsearch(key) : this.#seqsearch(key);    
  }

  find(key) {
    let idx = this.#sorted ? this.#binsearch(key) : this.#seqsearch(key);
    if (idx != null) {
      return this[idx];
    } else {
      return null;
    }
  }
  
  select(filterFunc) {
    let result = new this.#listClass();
    this.forEach((item) => { 
      if (filterFunc(item)) 
        result.push(item) 
    });
    return result;
  }

  copyFromObjectList(objectList) {
    this.clear();
    objectList.forEach((obj) => {
      let item = new this.#itemClass();
      if (item.copyFromObject) {
        item.copyFromObject(obj);
      } else {
        Object.assign(item,obj);
      }
      this.push(item);
    });
  }

  saveToFile(filename) {
    console.log("Saving:",filename);
    fs.writeFileSync(filename,JSON.stringify(this,null,"  "),{ encoding: 'utf8' });     
  }

  loadFromFile(filename) {
    console.log("Loading:",filename);
    this.clear();
    let buffer = fs.readFileSync(filename,{ encode: 'utf-8'});
    let objs = JSON.parse(buffer);
    buffer = null;
    this.copyFromObjectList(objs);
  } 

}

class DistrictList extends ObjectList {
  constructor() {
    super(District,DistrictList);
  }
}

class PollDivisionList extends ObjectList {
  constructor() {
    super(PollDivision,PollDivisionList);
  }
}

class EventList extends ObjectList {
  constructor () {
    super(Event,EventList);
  }
} 

class PartyList extends ObjectList {
  constructor() {
    super(Party,PartyList);
  }
}

class CandidateList extends ObjectList{
  constructor() {
    super(Candidate,CandidateList);
  }
}

class ElectionList extends ObjectList{
  constructor() {
    super(Election,ElectionList);
  }
}

class PollResultList extends ObjectList {
  constructor() {
    super(PollResult,PollResultList);
  }
}

class VoteList extends ObjectList {
  constructor() {
    super(Vote,VoteList);
  }
  
  get total() { 
    var result = 0;
    this.forEach((item) => result += item.count);
    return result;
  }
}

class DistrictGeometryList extends ObjectList {
  constructor() {
    super(DistrictGeometry,DistrictGeometryList);
  }
}

class PollDivisionGeometryList extends ObjectList {
  constructor() {
    super(PollDivisionGeometry,PollDivisionGeometryList);
  }
}

class DistrictGeometryIntersectList extends ObjectList {
  constructor() {
    super(DistrictGeometryIntersect,DistrictGeometryIntersectList);
  }
}

class PollDivisionGeometryIntersectList extends ObjectList {
  constructor() {
    super(PollDivisionGeometryIntersect,PollDivisionGeometryIntersectList);
  }
}

class Event {
  type;
  year;
  totalVotes;
  totalDeclined;
  totalUnmarked;
  totalRejected;
  numElectors;
  turnout;
  parties;
  districts;
  
  constructor (type,year) { 
    this.type = type; 
    this.year = year; 
    this.totalVotes = null;
    this.totalDeclined = null;
    this.totalUnmarked = null;
    this.totalRejected = null;
    this.numElectors = null;
    this.turnout = null;
    this.parties = new PartyList();
    this.districts = new DistrictList();
  }
  
  get isGeneral() { return this.type === 'GE'; }
  set isGeneral(value) { value ? this.type = 'GE' : this.type = 'BE'; }
  
  get isByelection() { return this.type === 'BE'; }
  set isByelection(value) { value ? this.type = 'BE' : this.type = 'GE'; }
  
  get code() { return this.type + this.year; }
  set code(value) { 
    type = value.substr(0,2);
    year = value.substr(2); 
  }
  
  get name() {
    return (this.type === 'GE') ? this.year + " General Election" : this.year + " Byelection";
  }

  clear() {
    this.parties.clear();
    this.districts.clear();
    super.clear();
  }

  compare(a,b) {
    var result = a.year - b.year;
    if (result == 0) 
      result = a.type.localeCompare(b.type);
    return result;
  }

  copyFromObject(obj) {
    this.type = obj.type;
    this.year = obj.year;
    this.totalVotes = obj.totalVotes;
    this.totalDeclined = obj.totalDeclined;
    this.totalUnmarked = obj.totalUnmarked;
    this.totalRejected = obj.totalRejected;
    this.numElectors = obj.numElectors;
    this.turnout = obj.turnout;
    this.parties.copyFromObjectList(obj.parties);
    this.districts.copyFromObjectList(obj.districts);
  }
}

class Party {
  code;
  name;
  registered;
  percent;
  votes;

  constructor (code,name,registered) {
    this.code = code;
    this.name = name;
    this.registered = registered;
    this.percent = null;
    this.votes = null;
  }

  compare(a,b) {
    return a.code.localeCompare(b.code);
  }
}

class District {
  number;
  name;

  constructor (number,name) {
    this.number = number;
    this.name = name;
  }

  compare(a,b) {
    return a.number - b.number;
  }

}

class Election {
  event;
  district;
  totalUnmarked;
  totalRejected;
  totalDeclined;
  totalVotes;
  numElectors;
  turnout;
  candidates;
  pollResults;

  get turnout() { return (this.totalVotes + this.totalDeclined) / this.numElectors * 100; }
  
  constructor (event,district) {
    this.event = event;
    this.district = district;
    this.totalUnmarked = null;
    this.totalRejected = null;
    this.totalDeclined = null;
    this.totalVotes = null;
    this.numElectors = null;
    this.turnout = null;
    this.candidates = new CandidateList();
    this.pollResults = new PollResultList();
  }

  clear() {
    this.candidates.clear();
    this.pollResults.clear();
    super.clear();
  }

  compare(a,b) {
    let result = a.event.year - b.event.year;
    if (result == 0) 
      result = a.event.type.localeCompare(b.event.type);
    if (result == 0) 
      result = a.district.number - b.district.number;
    return result;
  }

  copyFromObject(obj) {
    this.event = obj.event;
    this.district = obj.district;
    this.totalUnmarked = obj.totalUnmarked;
    this.totalRejected = obj.totalRejected;
    this.totalDeclined = obj.totalDeclined;
    this.totalVotes = obj.totalVotes;
    this.numElectors = obj.numElectors;
    this.turnout = obj.turnout;
    this.candidates.copyFromObjectList(obj.candidates);
    this.pollResults.copyFromObjectList(obj.pollResults);
  }

  saveToFile(filename) {
    console.log("Saving:",filename);
    let data = JSON.stringify(this,null,"  ");
    fs.writeFileSync(filename,data,{ encoding: 'utf-8'});
  }

  loadFromFile(filename) {
    console.log("Loading:",filename);
    let buffer = fs.readFileSync(filename,{ encoding: 'utf-8' });
    let obj = JSON.parse(buffer);
    buffer = null;
    this.copyFromObject(obj);
  }

}

class Candidate {
  name;
  party;
  votes;
  percent;
  incumbent;

  constructor (name,party,votes,percent,incumbent) {
    this.name = name;
    this.party = party;
    this.votes = votes;
    this.percent = percent;
    this.incumbent = incumbent;
  }
  
  compare(a,b) {
    return a.name.localeCompare(b.name);
  }

}

class PollResult {
  pollno;
  pollcode;
  taken;
  advance;
  combined;
  combinedWith;
  location;
  unmarked;
  rejected;
  declined;
  numElectors;
  totalVotes;
  turnout;
  votes;
  
  constructor (pollcode,taken,advance,combined,combinedWith,location,unmarked,rejected,declined,numElectors) {
    if (pollcode)
      this.pollno = this.getPollnoFromPollcode(pollcode);
    else
      this.pollno = null;
    this.pollcode = pollcode;
    this.taken = taken;
    this.advance = advance;
    this.combined = combined;
    this.combinedWith = combinedWith;
    this.location = location;
    this.unmarked = unmarked;
    this.rejected = rejected;
    this.declined = declined;
    this.numElectors = numElectors;
    this.totalVotes = null;
    this.turnout = null;
    this.votes = new VoteList();
  }

  getPollnoFromPollcode(value) {
    return Number(String(value).match(/\d+/)[0])
  }

  clear() {
    this.votes.clear();
    super.clear();
  }  

  compare(a,b) {
    let result = a.pollno - b.pollno;
    if (result == 0)
      result = a.pollcode.localeCompare(b.pollcode);
    return result;
  }

  copyFromObject(obj) {
    this.pollno = obj.pollno;
    this.pollcode = obj.pollcode;
    this.taken = obj.taken;
    this.advance = obj.advance;
    this.combined = obj.combined;
    this.combinedWith = obj.combinedWith;
    this.location = obj.location;
    this.unmarked = obj.unmarked;
    this.rejected = obj.rejected;
    this.declined = obj.declined;
    this.numElectors = obj.numElectors;
    this.totalVotes = obj.totalVotes;
    this.turnout = obj.turnout;
    this.votes.copyFromObjectList(obj.votes);
  }
}

class Vote {
  candidate;
  count;
  percent;
  
  constructor (candidate,count) {
    this.candidate = candidate;
    this.count = count;
    this.percent = null;
  }
    
  compare(a,b) { 
    return a.candidate.localeCompare(b.candidate);
  }
}

class DistrictGeometryIntersect {
  edno2014;
  edno2018;
  area;
  bbox;
  geom;
  
  constructor (edno2014, edno2018, area, bbox, geom) {
    this.edno2014 = edno2014;
    this.edno2018 = edno2018;
    this.area = area;
    this.bbox = bbox;
    this.geom = geom;
  }

  compare(a,b) {
    let result = a.edno2014 - b.edno2014;
    if (result == 0) 
      result = a.edno2018 - b.edno2018;
    return result;
  }

  copyFromObject(obj) {
    this.edno2014 = obj.edno2014;
    this.edno2018 = obj.edno2018;
    this.area = obj.area;
    this.bbox = obj.bbox;
    this.geom = obj.geom;
  }

}

class DistrictGeometry {
  edno;
  area;
  bbox;
  geom;
  
  constructor (edno,area,bbox,geom) {
    this.edno = edno;
    this.area = area;
    this.bbox = bbox;
    this.geom = geom;
  }

  compare(a,b) {
    return a.edno - b.edno;
  }

  copyFromObject(obj) {
    this.edno = obj.edno;
    this.area = obj.area;
    this.bbox = obj.bbox;
    this.geom = obj.geom;
  }

}

class PollDivisionGeometry {
  edno;
  pollno;
  area;
  geom;
  constructor (edno,pollno,area,bbox,geom) {
    this.edno = edno;
    this.pollno = pollno;
    this.area = area;
    this.bbox = bbox;
    this.geom = geom;
  }

  compare(a,b) {
    var result = a.edno - b.edno;
    if (result == 0)
      result = a.pollno - b.pollno;
    return result;
  }

  copyFromObject(obj) {
    this.edno = obj.edno;
    this.pollno = obj.pollno;
    this.area = obj.area;
    this.bbox = obj.bbox;
    this.geom = obj.geom;
  }

}

class PollDivisionGeometryIntersect {
  edno2014;
  pollno2014;
  edno2018;
  pollno2018;
  area;
  bbox;
  geom;

  constructor (edno2014, pollno2014, edno2018, pollno2018, area, bbox, geom) {
    this.edno2014 = edno2014;
    this.pollno2014 = pollno2014;
    this.edno2018 = edno2018;
    this.pollno2018 = pollno2018;
    this.area = area;
    this.bbox = bbox;
    this.geom = geom;
  }

  compare(a,b) {
    let result = a.edno2014 - b.edno2014;
    if (result == 0) 
      result = a.pollno2014 - b.edno2014;
    else
      return result;
    if (result == 0) 
      result = a.edno2018  - b.edno2018;
    else 
      return result;
    if (result == 0)
      result = a.pollno2018 - b.pollno2018;
    return result;
  }

  copyFromObject(obj) {
    this.edno2014 = obj.edno2014;
    this.pollno2014 = obj.pollno2014;
    this.edno2018 = obj.edno2018;
    this.pollno2018 = obj.pollno2018;
    this.area = obj.area;
    this.bbox = obj.bbox;
    this.geom = obj.geom;    
  }
}

class Database {
  events;
  elections;
  districts;
  districtGeometry2014;
  districtGeoetry2018;
  pollDivisionGeometry2014;
  pollDivisionGeometry2018;
  districtGeometryIntersects;
  pollDivisionGeometryIntersects;
  
  constructor() { 
    this.events = new EventList();
    this.elections = new ElectionList();
    this.districts = new DistrictList();
    this.districtGeometry2014 = new DistrictGeometryList();
    this.districtGeometry2018 = new DistrictGeometryList();
    this.pollDivisionGeometry2014 = new PollDivisionGeometryList();
    this.pollDivisionGeometry2018 = new PollDivisionGeometryList();
    this.pollDivisionGeometryIntersects = new PollDivisionGeometryIntersectList();
    this.districtGeometryIntersects = new DistrictGeometryIntersectList();
  }

  clear() {
    this.events.clear();
    this.elections.clear();
    this.districtGeometry2014.clear();
    this.districtGeometry2018.clear();
    this.pollDivisionGeometry2014.clear();
    this.pollDivisionGeometry2018.clear();
    this.pollDivisionGeometryIntersects.clear();
    this.districtGeometryIntersects.clear();
  }

  saveToDirectory(dir) {
    this.elections.forEach((election) => {
      election.saveToFile(path.join(dir,'elections',election.event.code+'-'+election.district.number+'.json'));
    });
    this.events.saveToFile(path.join(dir,'events.json'));
    this.districtGeometry2014.saveToFile(path.join(dir,'geometry','districtGeometry2014.json'));
    this.districtGeometry2018.saveToFile(path.join(dir,'geometry','districtGeometry2018.json'));
    this.pollDivisionGeometry2014.saveToFile(path.join(dir,'geometry','pollDivisionGeometry2014.json'));
    this.pollDivisionGeometry2018.saveToFile(path.join(dir,'geometry','pollDivisionGeometry2018.json'));
    this.districtGeometryIntersects.saveToFile(path.join(dir,'geometry','districtGeometryIntersects.json'))
    this.pollDivisionGeometryIntersects.saveToFile(path.join(dir,'geometry','pollDivisionGeometryIntersects.json'));
  }

  loadFromDirectory(dir) {
    let files = fs.readdirSync(path.join(dir,'elections'), { withFileTypes: true });
    this.elections.clear();
    files.forEach(function (file) {
      if ((file.isFile) && (path.extname(file.name) === '.json')) {
        let election = new Election();
        election.loadFromFile(path.join(dir,'elections',file.name));
        db.elections.push(election);
      }
    });

    this.events.loadFromFile(path.join(dir,'events.json'));
    this.districtGeometry2014.loadFromFile(path.join(dir,'geometry','districtGeometry2014.json'));
    this.districtGeometry2018.loadFromFile(path.join(dir,'geometry','districtGeometry2018.json'));
    this.pollDivisionGeometry2014.loadFromFile(path.join(dir,'geometry','pollDivisionGeometry2014.json'));
    this.pollDivisionGeometry2018.loadFromFile(path.join(dir,'geometry','pollDivisionGeometry2018.json'));
    this.districtGeometryIntersects.loadFromFile(path.join(dir,'geometry','districtGeometryIntersects.json'));
    this.pollDivisionGeometryIntersects.loadFromFile(path.join(dir,'geometry','pollDivisionGeometryIntersects.json'));
  }
}

var db = new Database();

module.exports = {
  db: db,
  Candidate,
  District,
  Election,
  Event,
  Party,
  PollResult,
  Vote,
  DistrictGeometry,
  PollDivisionGeometry,
  DistrictGeometryIntersect,
  PollDivisionGeometryIntersect
}
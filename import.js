var fs                = require('fs');
var path              = require('path');
var db                = require('./database').db;
var csvImporter       = require('./csvimporter');
var shpImporter       = require('./shpimporter');
var summaryCalculator = require('./summarycalculator');

fs.mkdirSync(path.join('.','json','elections'),{ recursive: true });
fs.mkdirSync(path.join('.','json','geometry'),{ recursive: true });

shpImporter.import();
csvImporter.import(() => {
  summaryCalculator.execute();                    // Comment this line out to not calculate vote totals
  db.saveToDirectory(path.join('.','json')); // Comment this line out if you don't want to generate database.json
});

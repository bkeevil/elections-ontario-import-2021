/** Exports the CSVImporter class which provides a means to 
 *    1) import the CSV files provided by Elections Ontario, converting 
 *       them to "records"
 *    2) to iterate through this datastore, finding related records and 
 *       passing them to an import callback function.
 *  
 *  The Database module provides a class with a method importRelation() 
 *  that should receive the callback from #2.
 * 
 *  Select which directories to import by adding lines to the directories
 *  object below.
 * 
 *  Warning: Elections Ontario database formats change frequently and this
 *  program may require you to update its code to adapt to those changes.
 */
var fs    = require('fs');
var path  = require('path');
var parse = require('csv-parse');
var db = require('./database').db;

const Candidate = require('./database').Candidate;
const District = require('./database').District;
const Election = require('./database').Election;
const Event = require('./database').Event;
const Party = require('./database').Party;
const PollDivision = require('./database').PollDivision;
const PollResult = require('./database').PollResult;
const Vote = require('./database').Vote;

const directories = [
/*  "1990 General Election-csv_2021-Feb-17",
  "1995 General Election-csv_2021-Feb-17",
  "1999 General Election-csv_2021-Feb-17",
  "2003 General Election-csv_2021-Feb-17",*/
  "2007 General Election-csv_2021-Feb-17",
  "2011 General Election-csv_2021-Feb-17",
  "2014 General Election-csv_2021-Feb-17",
  "2018 General Election-csv_2021-Feb-17",
];

/** The base class that provides common functionality for importing CSV files 
 *  and searching for records.  
 *  
 *  @todo Implement binary search on records to improve performance
 */
class CSVFile {
  records;
  #recordClass;
  #sorted;

  constructor (recordClass) {
    this.#recordClass = recordClass;
    this.records = [];
    this.#sorted = false;
  }

  clear() { 
    this.records = []; 
  }

  sort() { 
    if (!this.#sorted) {
      this.records.sort(this.#recordClass.prototype.compare); 
      this.#sorted = true;
    }
  }
  
  binsearch(key) {
    let start=0;
    let end=this.records.length - 1;        
    let mid=Math.floor((start + end)/2);
    let cr = this.#recordClass.prototype.compare(this.records[mid],key);
    while ((cr != 0) && (start < end)) {  
      if (cr > 0)
        end = mid - 1;
      else if (cr < 0)
        start = mid + 1;
      mid = Math.floor((start + end)/2);
      cr = this.#recordClass.prototype.compare(this.records[mid],key);
    } 
    return (cr != 0) ? null : mid; 
  }

  seqsearch(key) { 
    for (var idx = 0; idx < this.records.length; idx++) {
      if (this.#recordClass.prototype.compare(this.records[idx],key) == 0) {
        return idx;
      }
    }
    return null;
  }

  indexof(key) {
    return this.#sorted ? this.binsearch(key) : this.seqsearch(key);
  }

  find(key) {
    let idx = this.#sorted ? this.binsearch(key) : this.seqsearch(key);
    if (idx != null)
      return this.records[idx];
    else
      return null;
  }

  // Loads a CSV file from a filename into an a two dimensional array
  loadFromCSV(filename,callback) {  
    console.log(`Loading: ${filename}`);     
    fs.readFile(filename, { flag: "r"},(err,content) => {
      if (err) {
        console.error("Could not read the CSV file. ", err);
        callback(err);
      } else {
        parse(content.toString(),{ content: "utf8" },(err,output) => {
          if (err) {
            console.error('Could not parse the CSV file. ', err);
            callback(err);
          } else {
            var record;
            this.#sorted = false;
            for (var idx = 1; idx < output.length; idx++) {
              record = new this.#recordClass(output[idx]);
              if (this.#recordClass != PollRecord) {
                var i = this.indexof(record);
                if (i == null) {
                  this.records.push(record);
                } else {
                  this.records[i] = record;
                }
              } else {
                this.records.push(record);
              }
            }
            callback(null);
          }
        });
      }
    });
  }
}

/** Represents a candidate record
 * 
 *  inbumbent means the candidate was a member of the previous legislature.
 */
class CandidateRecord {
  eventType;
  year;
  edno;
  edname;
  name;
  pic;
  totalVotes;
  percentvote;
  incumbent;

  constructor(row) {
    this.eventType = Number(row[2]) == 0 ? "BE" : "GE";
    this.year = Number(String(row[8]).substr(0,4));
    this.edno = Number(row[3]);
    this.edname = String(row[4]);
    this.name = String(row[10]);
    this.pic = String(row[11]);
    this.totalVotes = Number(row[12]);
    this.percentvote = Number(row[13]);
    this.incumbent = Boolean(Number(row[15]));
  }

  compare(a,b) {
    var result;
    result = a.year - b.year;
    if (result == 0) {
      result = a.eventType.localeCompare(b.eventType);
    } else 
      return result;
    if (result == 0) {
      result = a.edno - b.edno;
    } else 
      return result;
    if (result == 0) {
      return a.name.localeCompare(b.name);
    } else 
      return result;
  }

}

/** Data from the "Valid Votes Cast for Each Candidate.csv" file converted into records */
class CandidatesCSVFile extends CSVFile {
  constructor() { super(CandidateRecord); }
}

/** Represents a political party record. 
 * 
 *  PIC = Political Interest Code 
 */
class PartyRecord {
  eventType;
  year;
  pic;
  name;
  registered;

  constructor(row) {
    this.year = Number(String(row[1]).substr(0,4));
    this.eventType = (String(row[1]).includes("General Election")) ? "GE" : "BE";
    this.pic = String(row[2]);
    this.name =  String(row[3]);
    this.registered = Boolean(row[5]);
  }

  compare(a,b) {
    var result;
    result = a.year - b.year;    
    if (result == 0) {
      result = a.eventType < b.eventType ? -1 : +(a.eventType > b.eventType);
    } else 
      return result;
    if (result == 0) {
      return a.pic < b.pic ? -1 : +(a.pic > b.pic)       
    } else 
      return result; 
  }
}

/** Data from the "Political Interest Codes.csv" files converted to records */
class PartiesCSVFile extends CSVFile {
  constructor() { super(PartyRecord); }
}

/** Represents a poll record */
class PollRecord {
  eventType;
  year;
  edno;
  edname;
  pollno;
  taken;
  advance;
  combined;
  combinedWith;
  rejected;
  unmarked;
  declined;
  numElectors;
  location;
  candidate;

  constructor(row) {
    //this.event = String(row[0]);
    this.eventType = String(row[0]).includes("General Election") ? "GE" : "BE";
    this.year = Number(String(row[0]).substr(0,4));
    this.edno = Number(String(row[2]).substr(0,3));
    this.edname = String(row[2]).substr(4);
    this.advance = Boolean(String(row[4]).includes("Advance"));
    this.pollno = String(row[5]);
    this.taken = Boolean(Number(row[7]) == 0);
    this.combined = Boolean(Number(row[8]) == 1);
    this.combinedWith = String(row[9]);
    this.rejected = Number(row[10]);
    this.unmarked = Number(row[11]);
    this.declined = Number(row[12]);
    this.numElectors = Number(row[13]); 
    this.location = String(row[14]);
    this.candidate = this.fixReversedName(String(row[15]));
    this.votes = Number(row[16]);
  }

  fixReversedName(name) {
    var i = name.lastIndexOf(',');
    if (i == -1) {
      return name;
    } else {
      return name.substr(i+2) + ' ' + name.substr(0,i);
    }
  }  

  compare(a,b) {
    var result;
    result = a.year - b.year;
    if (result == 0) {
      result = a.eventType < b.eventType ? -1 : +(a.eventType > b.eventType);
    } else return result;
    if (result == 0) {
      result = a.edno - b.edno;
    } else return result;
    if (result == 0) {
      return a.pollno < b.pollno ? -1 : +(a.pollno > b.pollno)       
    }
  }
}

/** Data from the "Official Return from the Records.csv" files converted to objects */
class PollsCSVFile extends CSVFile {
  constructor() { super(PollRecord); }
}

/** Imports data from the CSV files provided by Elections Ontario and converts
 *  them to three arrays of records that are stored in memory. 
 * 
 *  Use loadDirectories() to iterate through the directories[] array and load
 *  data from relevant CSV files into memory.
 *  
 *  Use findRelations() to iterate through poll records, looking up the associated 
 *  candidate and party and then passing all three records to a callback function.
 */
class CSVImporter {
  candidates;
  parties;
  polls;

  constructor() {
    this.candidates = new CandidatesCSVFile();
    this.parties = new PartiesCSVFile();
    this.polls = new PollsCSVFile();
  }

  import(callback) {
    var filename;
    var count = directories.length;
    directories.forEach((directory) => {
      filename = path.join('.','Elections Ontario Data',directory,"Political Interest Codes.csv");
      this.parties.loadFromCSV(filename,(err) => {
        if (err) {
          console.error(`Could not load file ${filename}`);
        } else if (--count == 0) {
          this.parties.sort();
          console.log(`Loaded ${this.parties.records.length} party records`);
          this.importEventsAndParties();
          this.parties.clear();
          this.sortdbEventsAndParties();
          count = directories.length;
          directories.forEach((directory) => {
            filename = path.join('.','Elections Ontario Data',directory,"Valid Votes Cast for Each Candidate.csv");
            this.candidates.loadFromCSV(filename,(err) => {
              if (err) {
                console.error(err);
              } else if (--count == 0) {
                this.candidates.sort();
                console.log(`Loaded ${this.candidates.records.length} candidates records`);
                this.importDistrictsElectionsAndCandidates();
                this.candidates.clear();
                this.sortdbDistrictsElectionsAndCandidates();
                count = directories.length;
                directories.forEach((directory) => { 
                  filename = path.join('.','Elections Ontario Data',directory,"Official Return from the Records.csv");
                  this.polls.loadFromCSV(filename,(err) => {
                    if (err) {
                      console.error(err);
                    } else if (--count == 0) {
                      this.polls.sort();
                      console.log(`Loaded ${this.polls.records.length} polls records`);
                      this.importPollsAndVotes();
                      this.polls.clear();
                      this.sortdbPollsAndVotes();
                      callback();
                    }
                  });
                });
              }
            });
          });
        }
      });
    });
  }

  importEventsAndParties() {
    console.log("Importing Events and Parties");
    this.parties.records.forEach((partyRec) => {
      let event = this.importCSVEvent(partyRec);
      this.importCSVParty(event,partyRec);
    });
  }

  sortdbEventsAndParties() {
    console.log("Sorting Events and Parties")
    db.events.sort();
    db.events.forEach((event) => event.parties.sort());
  }

  importDistrictsElectionsAndCandidates() {
    let event;
    console.log("Importing Districts, Elections and Candidates");
    this.candidates.records.forEach((candidateRec) => {
      // Lookup the event using candidate fields
      let eventKey = { type: candidateRec.eventType, year: candidateRec.year };
      if (!event || (event.compare(event,eventKey) != 0))
        event = db.events.find(eventKey);
      // Import the district record
      let district = this.importCSVDistrict(event,candidateRec);
      let election = this.importCSVElection(event,district);
      let candidate = this.importCSVCandidate(election,candidateRec);
    });
  }

  sortdbDistrictsElectionsAndCandidates() {
    console.log("Sorting Elections and Candidates");
    db.districts.sort();
    db.elections.sort();
    db.elections.forEach((election) => election.candidates.sort());
  }

  importPollsAndVotes() {
    let lastPercent = 0;
    let election;
    console.log("Importing Polls and Votes");
    this.polls.records.forEach((pollRec,idx) => {
      let percent = Math.floor(idx / this.polls.records.length * 100);
      if (percent != lastPercent) 
        console.log(`${lastPercent = percent}% complete`);
      
      let electionKey = { event: { year: pollRec.year, type: pollRec.eventType }, district: { number: pollRec.edno }};
      
      // Retrieve event, district and dbpoll.
      // Check to see if the item has changed before performing a search
      if (!election || (election.compare(election,electionKey) != 0)) {
        election = db.elections.find(electionKey);
      }
      
      let dbpoll = this.importCSVPoll(election,pollRec);
      dbpoll.votes.push(new Vote(pollRec.candidate,pollRec.votes));
    });
  }

  sortdbPollsAndVotes() {
    console.log("Sorting Polls and Votes")
    db.elections.forEach((election) => {
      election.pollResults.sort();
      election.pollResults.forEach((pr) => pr.votes.sort());
    });
  }

  importCSVEvent(partyRec) {
    var result = db.events.find({ type: partyRec.eventType, year: partyRec.year });
    if (!result) {
      result = new Event(partyRec.eventType,partyRec.year);
      db.events.push(result);
    }
    return result;
  }

  importCSVParty(event,partyRec) {
    var result = event.parties.find({ code: partyRec.pic });
    if (!result) {
      result = new Party(partyRec.pic,partyRec.name,partyRec.registered);
      event.parties.push(result);
    }
    return result;
  }

  importCSVDistrict(event,candidateRec) {
    let result = event.districts.find({ number: candidateRec.edno });
    if (!result) {
      result = new District(candidateRec.edno, candidateRec.edname);
      event.districts.push(result);
    }
    return result;  
  }

  importCSVElection(event,district) {
    var result = db.elections.find({ event: { year: event.year, type: event.type }, district: { number: district.number }});
    if (!result) {
      result = new Election(event,district);
      db.elections.push(result);
    }
    return result;    
  }

  importCSVCandidate(election,candidateRec) {
    var result = election.candidates.find(candidateRec);
    if (!result) {
      result = new Candidate(candidateRec.name,candidateRec.pic,candidateRec.totalVotes,candidateRec.percentvote,candidateRec.incumbent);
      election.candidates.push(result);
    }
    return result;    
  }

  importCSVPoll(election,pollRec) {
    var result = election.pollResults.find({ pollno: PollResult.prototype.getPollnoFromPollcode(pollRec.pollno), pollcode: pollRec.pollno });
    if (!result) {
      var result = new PollResult(pollRec.pollno,pollRec.taken,pollRec.advance,pollRec.combined,pollRec.combinedWith,
        pollRec.location,pollRec.unmarked,pollRec.rejected,pollRec.declined,pollRec.numElectors);
      election.pollResults.push(result);
    }
    return result;    
  }

};

module.exports = new CSVImporter();